module gitlab.com/brandonbutler/chiaupdate

go 1.16

require (
	github.com/google/go-github/v44 v44.0.0
	github.com/hashicorp/go-version v1.4.0
	github.com/spf13/cobra v1.1.3
)
