/*
Copyright © 2022 Brandon Butler <bmbawb@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"log"
	"os/exec"
	"time"

	version "github.com/hashicorp/go-version"
	"github.com/spf13/cobra"
	"gitlab.com/brandonbutler/chiaupdate/internal/release"
)

var (
	sleepTime = 60
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "chiaupdate",
	Short: "Looks for a new release and runs an unattended upgrade when found",

	Run: func(cmd *cobra.Command, args []string) {
		//Get latest release for the first time
		previousLatest, err := release.GetLatest()
		if err != nil {
			log.Fatal(err)
		}

		for {
			//Get latest release
			latest, err := release.GetLatest()
			if err != nil {
				log.Println(err)
				time.Sleep(time.Duration(sleepTime) * time.Second)
				continue
			}

			//Handle condition -- latest release unchanged
			if latest == previousLatest {
				log.Println("[INFO] Iteration found same release")
				time.Sleep(time.Duration(sleepTime) * time.Second)
				continue
			}

			//Handle condition -- latest API returns an older response, potentially due to cacheing issues
			if !isSemverNewer(previousLatest, latest) {
				log.Printf("[WARN] API returned a semver that is older than the current latest.\n\tCurrent: %s\n\tLatest API response: %s", previousLatest, latest)
				time.Sleep(time.Duration(sleepTime) * time.Second)
				continue
			}

			log.Println("[INFO] New latest release discovered: ", latest)

			// apt update
			cmd := exec.Command("apt", "update")
			stdout, err := cmd.Output()
			if err != nil {
				log.Fatalf("[ERROR] Received error running apt update: \n\t%v", err)
			}
			log.Println(string(stdout))

			// Stop chia
			cmd = exec.Command("systemctl", "stop", "chia.service")
			stdout, err = cmd.Output()
			if err != nil {
				log.Fatalf("[ERROR] Received error stopping chia: \n\t%v", err)
			}
			log.Println(string(stdout))

			// apt upgrade chia
			cmd = exec.Command("apt", "upgrade", "-y", "chia-blockchain-cli")
			stdout, err = cmd.Output()
			if err != nil {
				log.Fatalf("[ERROR] Received error running apt upgrade: \n\t%v", err)
			}
			log.Println(string(stdout))

			// re-start chia service
			cmd = exec.Command("systemctl", "start", "chia.service")
			stdout, err = cmd.Output()
			if err != nil {
				log.Fatalf("[ERROR] Received error re-starting Chia service: \n\t%v", err)
			}
			log.Println(string(stdout))

			//Update the old latest
			previousLatest = latest
			time.Sleep(time.Duration(sleepTime) * time.Second)
		}
	},
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func isSemverNewer(previous string, latest string) bool {
	v1, err := version.NewVersion(previous)
	if err != nil {
		log.Printf("[ERROR] isSemverNewer: previous version parsing returned an error: %v", err)
	}
	v2, err := version.NewVersion(latest)
	if err != nil {
		log.Printf("[ERROR] isSemverNewer: newest version parsing returned an error: %v", err)
	}

	return v1.LessThan(v2)
}
