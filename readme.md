# chiaupdate

This utility waits for releases and then automatically runs a few commands to stop your farm, update Chia, and re-start it again.

This project is not affiliated with the Chia blockchain, and is a fan-made project.

This is not tested software. I run it on my home Chia farm which is why I made it public but if it tears apart your farm, do not blame me.

# Pre-requisites

You need the Chia apt repository configured, as documented: https://github.com/Chia-Network/chia-blockchain/wiki/INSTALL#install-using-the-repository

You should currently be farming using the version of Chia installed via this apt repository.

# Use

Steal the binary from the latest release and copy it somewhere like `/usr/local/bin`.

You will need a systemd service created for both Chia and this Chiaupdate utility. You can create these by copying the two `*.service` files in this repo to `/etc/systemd/system/` and running `systemctl daemon-reload`

Once you do this, start and enable both services with: 

```
systemctl enable chia.service
systemctl start chia.service
systemctl enable chiaupdate.service
systemctl start chiaupdate.service
```

This will have started your farm as well as the chiaupdate service. You can check the status of chiaupdate with `systemctl status chiaupdate`