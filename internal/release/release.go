package release

import (
	"context"
	"fmt"

	"github.com/google/go-github/v44/github"
)

var (
	client *github.Client
)

const (
	repoOwner = "chia-network"
	repoName  = "chia-blockchain"
)

func init() {
	c := github.NewClient(nil)
	client = c
}

//GetLatest queries an API to receive the latest release
func GetLatest() (string, error) {
	release, _, err := client.Repositories.GetLatestRelease(context.TODO(), repoOwner, repoName)
	if err != nil {
		return "", fmt.Errorf("[ERROR] GetLatest: %v", err)
	}

	return *release.TagName, nil
}
